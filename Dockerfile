# Use the official GoLang image as the base image
FROM golang:1.17-alpine

# Added environment
ENV GO111MODULE=on
ENV GOPATH /go
ENV APP_HOME $GOPATH/src/app

# Create home directory
RUN mkdir -p "$APP_HOME"

# Set the working directory inside the container
WORKDIR "$APP_HOME"

# Copy the Go module files to the working directory
COPY go.mod go.sum ./

# # Download and install the Go module dependencies
RUN go mod download

# Install the Go module dependencies when the Go module is not installed
# RUN go mod init
# RUN go get github.com/gin-gonic/gin

# Copy the rest of the application source code to the working directory
COPY . .

# Build the Go app
# RUN go build -o myapp

# Expose port 8080 to the outside world
EXPOSE 8080

# Run the executable
CMD ["go", "run", "main.go"]
