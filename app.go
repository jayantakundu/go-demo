package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Print from the Go program")
	for {
		fmt.Println("Hello World")
		time.Sleep(time.Second * 3)
	}   
}